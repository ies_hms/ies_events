
(function () {
    'use strict';
    var App = angular.module('app');
    App.service('eventService', eventService);
    eventService.$inject = ['$http', '$resource'];
    /**
     * RegExp when serialized as part of JSON.stringify gives empty object. This function is to avoid it and send it as RegExp string
     * @param {*} key 
     * @param {*} value 
     */
    function stringifyFilter(key, value) {
        if (value instanceof RegExp) {
            return value.toString();
        }
        return value;
    }
    /**
     * student service to perform CRUD operation on students. As the list of students are less, no need to search on the server side. Search will be done on client side by caching the list of students
     *  
     * @param {*} resource 
     */
    function eventService($http, $resource) {
        this.getAllEvents = function (callback) {
            var eventIdentity = $resource("events");
            var eventInfo = eventIdentity.query(function (response) {
                callback(null, response);
            }, function (error) {
                callback(error, null);
            });
        };
        this.getEventDetails = function (id, callback) {
            var eventIdentity = $http.get('events/' + id);
            var eventInfo = eventIdentity.then(function (response) {
                callback(null, response);
            }, function (error) {
                callback(error, null);
            });
        };
        this.getEventsByOrganizerBody=function(value,callback){
            var eventIdentity = $http.get('events/orgBody/' + value);
            var eventInfo = eventIdentity.then(function (response) {
                callback(null, response);
            }, function (error) {
                callback(error, null);
            });
        };
        this.createEvent = function (values,callback) {
            var responsePromise = $http({
                method: 'POST',
                data: JSON.stringify(values),
                headers: { 'Content-Type': 'application/json' },
                url: 'events'
            });
            responsePromise.then(function (responseData) {
                callback(null, responseData.data);
            }, function (error) {
                callback(error, null);
            });
        };
        this.postComment = function (id, values) {
            var responsePromise = $http({
                method: 'post',
                data: JSON.stringify(values),
                headers: { 'Content-Type': 'application/json' },
                url: 'events/comment/' + id
            });
            responsePromise.then(function (responseData) {
                //callback(null, responseData.data);
            }, function (error) {
                console.log(error);
                //callback(error, null);
            });
        };
        this.editEvent = function (id,values,callback){
            var responsePromise = $http({
                method: 'put',
                data: JSON.stringify(values),
                headers:{ 'Content-Type':'application/json'},
                url:'events/updateEvent/'+id
            });
            responsePromise.then(function(responseData){
                callback(null,responseData.data);
            },function(error){
                console.log(error);
                callback(error,null);
            });
        };
        this.getEventById = function (id, callback) {
            var eventIdentity = $resource("events/" + id);
            var eventInfo = eventIdentity.query(function (response) {
                callback(null, response);
            }, function (error) {
                callback(error, null);
            });
        };
        this.getVenueLength = function(type,sdate,stime,callback){
            var eventIdentity = $resource("events/eventByVenue/"+type+"/"+sdate+"/"+stime);
            eventIdentity.query(function(response){
                callback(null,response.length);
            },function(error){
                callback(error,null);
            });
        };
        this.getFilter = function (type, callback) {
            var categoryIdentity = $resource("filter/" + type);
            var categoryInfo = categoryIdentity.query(function (response) {
                callback(null, response[0]);
            }, function (error) {
                callback(error, null);
            });
        };
        this.createFilter = function(value,callback){
            var details={ type: value,values:[]};
            var responsePromise = $http({
                method: 'post',
                data: JSON.stringify(details),
                headers: { 'Content-Type': 'application/json' },
                url: 'filter/'
            });
            responsePromise.then(function(response){
                callback(response,null);
            },function(error){
                callback(null,error);
            });
        };
        this.getDistinctValues = function(value,callback){
            var categoryIdentity = $resource("events/distinctFilters/" + value);
            var categoryInfo = categoryIdentity.query(function (response) {
                callback(null, response);
            }, function (error) {
                callback(error, null);
            });
        };
        this.getEventsByOrgBody = function(type,value,callback){
            var categoryIdentity = $resource("events/orgBy/" + value);
            var categoryInfo = categoryIdentity.query(function (response) {
                callback(null, response);
            }, function (error) {
                callback(error, null);
            });
        };
        this.getEventsByCategory = function(type,value,callback){
            var categoryIdentity = $resource("events/category/"+value);
            var categoryInfo = categoryIdentity.query(function (response){
                callback(null,response);
            },function(error){
                callback(error,null);
            });
        };
        this.getNestedDistinctValues = function(type,key,value,callback){
            var categoryIdentity = $resource("events/distinctFilters/" + type+"/"+key+"/"+value);
            var categoryInfo = categoryIdentity.query(function (response) {
                callback(null, response);
            }, function (error) {
                callback(error, null);
            });
        };
        this.addFilter = function(type,details){
            var values={values:details};
            var responsePromise = $http({
                method: 'put',
                data: JSON.stringify(values),
                headers: { 'Content-Type': 'application/json' },
                url: 'filter/' + type
            });
            responsePromise.then(function (responseData) {
                console.log(responseData);
            }, function (error) {
                console.log(error);
                
            });
        }
        this.removeAttachedDisplayPicture = function (value) {
            var responsePromise = $http({
                method: 'delete',
                data: JSON.stringify(value),
                headers: { 'Content-Type': 'application/json' },
                url: 'events/displayPicture'
            });
            responsePromise.then(function (response) {
                console.log(response);
                //callback(null, responseData.data);
            }, function (error) {
                console.log(error);
                //callback(error, null);
            });
        };
        this.getStudents=function(value,callback){
            var categoryIdentity = $resource("events/studentDets/" + value);
            var categoryInfo = categoryIdentity.query(function (response) {
                callback(null, response);
            }, function (error) {
                callback(error, null);
            });
        }
        this.getFaculty=function(value,callback){
            var categoryIdentity = $resource("events/facultyDets/" + value);
            var categoryInfo = categoryIdentity.query(function (response) {
                callback(null, response);
            }, function (error) {
                callback(error, null);
            });
        }
        this.getEventsByDate = function(startdate,callback){
            var categoryIdentity = $resource("events/report/"+startdate);
            var categoryInfo = categoryIdentity.query(function(response){
                callback(null,response);
            },function(error){
                callback(error,null);
            });
        }
        this.orderAgenda = function (event) {
            var values = event.agendaList;
            var new_array = [];
            event.noOfDays = moment(event.end).diff(moment(event.start), 'days');
            //calculating day
            for (i in values) {
                values[i].day = moment(values[i].start).diff(moment(event.start), 'days');
                values[i].startdate = moment(values[i].start).format('LL');
                values[i].starttime = moment(values[i].start).format('LT');
                values[i].endtime = moment(values[i].end).format('LT');
            }
            //sort by days ascending
            values.sort(function (a, b) {
                return a.days - b.days
            });
            for (var i = 0; i <= event.noOfDays; i++) {
                var query = { day: i+1, vals: [] };
                new_array[i] = query;
                for (var j in values) {
                    if (i == values[j].day) {
                        new_array[i].vals.push(values[j]);
                    }
                    
                }
                if (new_array[i].vals.length == 0) {
                    new_array.splice(i,1);
                }
            }
            console.log(new_array);
            return new_array;
        };
    }
    App.service('gridfsService', gridfsService);
    gridfsService.$inject = ['$resource', '$http'];

    function gridfsService($resource, $http) {
        this.removeDirtyAttachments = function (files, callback) {
            var responsePromise = $http({
                method: 'POST',
                data: JSON.stringify({ files: files }),
                headers: { 'Content-Type': 'application/json' },
                url: 'events/dirty/attachments'
            });

            responsePromise.then(function (responseData) {
                callback(null, responseData.data);
            }, function (error) {
                callback(error, null);
            });
        };
    }

    App.service('lookupService', lookupService);
    lookupService.$inject = ['$resource', '$http'];

    /**
     * Lookup service for all drop downs
     *  
     * @param {*} resource 
     */
    function lookupService($resource, $http) {

        this.loadDataFields = function (keyName, callback) {
            var lookupIdentity = $resource("lookups/load/:keyName");
            var lookupInfo = lookupIdentity.query({ keyName: keyName }, function (response) {
                callback(null, response);
            }, function (error) {
                callback(error, null);
            });
        }

        this.addNewItemsIntoLookup = function (keyName, newValues, callback) {
            var lookupResource = $resource("lookups/:keyName", null, {
                'addValue': { method: 'PUT' }
            });

            var lookupInfo = lookupResource.addValue({ keyName: keyName }, { values: newValues }, function (response) {

                callback(null, response);
            }, function (error) {
                callback(error, null);
            });
        }


        this.loadValidationRulesForAllForms = function (callback) {
            var lookupIdentity = $resource("lookups/validation/rules/messages");
            var lookupInfo = lookupIdentity.get(function (response) {
                callback(null, response);
            }, function (error) {
                callback(error, null);
            });
        }

    }

})();
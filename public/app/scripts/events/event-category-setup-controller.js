(function(){
    'use strict';
    var App = angular.module('app');
    App.controller('EventCategorySetupCtrl', eventCategorySetupController);
    eventCategorySetupController.$inject = ['$scope', '$localStorage', '$window', 'eventService'];
    function eventCategorySetupController($scope, $localStorage, $window, eventService) {
        eventService.getFilter('association-categories', function (err, result) {
            if (!err) {
                $scope.associationCategories = result;
            }
        });
        eventService.getFilter('club-categories', function (err, result) {
            if (!err) {
                $scope.clubCategories = result;
            }
        });
        eventService.getFilter('institution-categories',function(err,result){
            if(!err){
                $scope.institutionCategories = result;
            }
        });
        eventService.getFilter('trust-categories',function(err,result){
            if(!err){
                $scope.trustCategories = result;
            }
        });
        eventService.getFilter('external-categories',function(err,result){
            if(!err){
                $scope.externalCategories = result;
            }
        });
        $scope.setupCategory= function(values){
            $scope.selectedType=values;
        };
        $scope.editCategory=function(details){
            eventService.addFilter(details.type, details.values);
        }
        $scope.createCategory = function(value){
            eventService.createFilter(value+"s",function(result,err){
                if(!err){
                    console.log(result);
                }
                else{
                    console.log(err);
                }
            });
            eventService.createFilter(value+"-categories",function(result,err){
                if(!err){
                    console.log(result);
                }
                else{
                    console.log(err);
                }
            });
            eventService.getFilter(value+'-categories',function(err,result){
                if(!err){
                    $scope.selectedType = result;
                    $scope[value+'Categories'] = result;
                }
            });     
            
        }
    }
})();
(function(){
    'use strict';
    var App = angular.module('app');
    App.controller('EventDetailCtrl', eventsDetailController);
    eventsDetailController.$inject = ['$scope','$http','$localStorage','$window', '$stateParams','$sce', 'eventService'];
    function eventsDetailController($scope, $http, $localStorage, $window, $stateParams, $sce, eventService) {
        eventService.getEventDetails($stateParams.id,function(err,result){
            if(!err){
                console.log(result);
                $scope.det=result.data;
            }
            else{
                console.log(err);
            }
        });
        $scope.withoutSanitize = function () {

            return $sce.trustAsHtml(event.fdesc);

        };
        $scope.withoutSanitize = function(){
            return $sce.trustAsHtml(event.eventPostSummary);
        };
        $scope.postComment = function (comment1) {
            var coms=[];
            var commentset = { time:moment().format('MMMM Do YYYY, h:mm:ss a'),comment:comment1}
            coms=JSON.parse(angular.toJson($scope.det.comment));
            coms.push(commentset);
            console.log(coms);
            eventService.postComment($scope.det._id,coms);
            $http.get('/events/' + $stateParams.id).then(function (response) {
                $scope.det.comment = response.data.comment;
            });
        };
        

    }
})();
(function () {
    'use strict';
    var App = angular.module('app');
    App.controller('EventCreateCtrl', eventsCreateController);
    eventsCreateController.$inject = ['$scope', '$localStorage', '$window', 'eventService', 'gridfsService'];
    function eventsCreateController($scope, $localStorage, $window, eventService, gridfsService) {
        $scope.event = {};
        $scope.event.coordinators = [{ role: "" }];
        $scope.groupQuery = " ";
        $scope.loadGroups = function (details) {
            if(details=="external"){
                eventService.getFilter('external-categories',function(err,result){
                    // $scope.groups=[];
                    // $scope.groups[0]={name:"external",categories:result};
                    $scope.event.orgBy={categories:result.values};
                });
            }
            else{
            eventService.getFilter(details, function (err, result) {
                if (!err) {
                    console.log(result);
                    $scope.groups = result.values;
                }
            });
            }
        };
        eventService.getFilter('venues', function (err, result) {
            if (!err) {
                $scope.venues = result.values;
            }
        });
        $scope.displayPicture = {
            dzOptions: {
                dictDefaultMessage: "Drop your the Event Display Pictures here",
                url: "/events/uploadPicture",
                method: "put",
                parallelUploads: 1,
                acceptedFiles: 'image/jpeg, images/jpg, image/png',
                autoProcessQueue: true,
                uploadMultiple: true,
                addRemoveLinks: true,
                maxFileSize: 5120,
                paramName: function () {
                    return "displayPicture";
                },
                maxFiles: 10,
                createImageThumbnails: true,
                params: "Values For Gallery",
                renameFile: function (file) {
                    file.upload.filename = file.name;
                }
            },
            dzCallbacks: {
                init: function () {
                    this.on("addedfile", function (file) {
                        alert("Added file.");
                    });
                },
                "sending": function (file, xhr, formData) {

                },
                "addedfile": function (file) {
                    $scope.isDisplayPicture = true;
                    $scope.lastAddedFile = file;
                },
                "success": function (file, xhr) {
                    $scope.showDpLoader = false;
                    $scope.showUploadDP = false;
                    $scope.picDets = xhr.displayPicture;
                    //loadDisplayPicture({ originalname: file.name });
                },
                "removedfile": function (file) {
                    eventService.removeAttachedDisplayPicture($scope.picDets);
                    $scope.isDisplayPicture = false;
                }
            },
            dzMethods: {

            }
        };
        $scope.agendalist = [];
        $scope.agenda={guests:[{}]};
        $scope.event.sponsors = [];
        $scope.addAgenda = function (details) {
            details.day = moment(details.start).diff(moment(event.start), 'days')+1;
            details.startdate = moment(details.start).format('LL');
            details.starttime = moment(details.start).format('LT');
            details.endtime = moment(details.end).format('LT');
            $scope.agendalist.push(details);
            $scope.agenda={guests:[{}]};
        };
        $scope.setupAgenda = function (index) {
            $scope.selectedIndex = index;
            $scope.agenda = $scope.agendalist[index];
        };
        $scope.editAgenda = function (details) {
            details.day = moment(details.start).diff(moment(event.start), 'days')+1;
            details.startdate = moment(details.start).format('LL');
            details.starttime = moment(details.start).format('LT');
            details.endtime = moment(details.end).format('LT');
            $scope.agendalist[$scope.selectedIndex].push(details);
        };
        $scope.removeAgenda = function (index) {
            $scope.agendalist.splice(index, 1);
        }
        // $scope.addAgenda = function () {
        //     var newItemNo = $scope.agendalist.length + 1;
        //     $scope.agendalist.push({ "no": "a" + newItemNo, "start": "", "end": "", "title": "", "summary": "", guest: [{}], "speaker": "", "venue": "" });
        // };
        $scope.addGuest = function (index) {
            $scope.agenda.guests.push({});
        }
        $scope.removeGuest = function (currentIndex) {
            $scope.agenda.guests.splice(currentIndex, 1);
        }
        $scope.addSponsor = function (details) {
            $scope.event.sponsors.push(details);
            $scope.sponsor={};
        }
        $scope.removeSponsor = function(index){
            $scope.event.sponsors.splice(index,1);
        }
        var initValidationEventcreate = function () {
            jQuery('.js-validation-bootstrap').validate({
                ignore: [],
                errorClass: 'help-block animated fadeInDown',
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    jQuery(e).parents('.form-group > div').append(error);
                },
                highlight: function (e) {
                    var elem = jQuery(e);
                    elem.closest('.form-group').removeClass('has-error').addClass('has-error');
                    elem.closest('.help-block').remove();
                },
                success: function (e) {
                    var elem = jQuery(e);

                    elem.closest('.form-group').removeClass('has-error');
                    elem.closest('.help-block').remove();
                },
                rules: {
                    'eventTitle': {
                        required: true
                    },
                    'eventLocation': {
                        venueValidation: true
                    }/*
                    'example-datetimepicker2': {
                        required: true,
                        eventDateChecker:true
                    },
                    'example-datetimepicker1': {
                        required: true,
                        eventDateChecker:true
                    },
                    'eventType': {
                        required:true,
                        NotNull: true
                    },
                    'eventTopic': {
                        required:true,
                        NotNull: true
                    },
                    'eventDesc': {
                        required: true
                    },
                    'eventOrgInst' : {
                        required : true,
                        NotNull : true
                    },
                    'eventOrgName': {
                        required: true
                    },
                    'eventOrgPhone' : {
                        required : true,
                        phoneUS : true
                    },
                    'eventOrgEmail' : {
                        required : true,
                        email : true
                    },
                    'eventCost' : {
                        required : true
                    },
                    'eventAgendaStart' : {
                        required : true,
                        AgendaDateChecker : true
                    },
                    'eventAgendaEnd' : {
                        required : true,
                        AgendaDateChecker : true
                    },
                    'eventAgendaTitle' : {
                        required : true
                    },
                    'eventAgendaSummary' : {
                        required : true
                    },
                    'eventAgendaVenue' : {
                        required : true,
                        NotNull : true
                    },
                    'radios2' : {
                        required : true
                    },
                    'eventSharing' : {
                        required : true
                    }*/
                },
                messages: {
                    'eventTitle': 'Please enter Event Title',
                    'eventLocation': {
                        venueValidation: 'Please Select another location'
                    }/*,
                    'example-datetimepicker2': {
                        required: 'Please select start date and time',
                        eventDateChecker: 'Please Enter valid Dates'
                    },
                    'example-datetimepicker1': {
                        required:'Please select end date and time',
                        eventDateChecker:'Please Enter valid Dates'
                    },
                    'eventType': {
                        required:'Please select event type',
                        NotNull: 'Please select event type'
                    },
                    'eventTopic': {
                        required:'Please select event Topic',
                        NotNull: 'Please select event Topic'
                    },
                    'eventDesc': 'Please provide a small description about the event',
                    'eventOrgInst' : {
                        required:'Please select institution',
                        NotNull: 'Please select institution'
                    },
                    'eventOrgName': 'Please provide the organizer name',
                    'eventOrgPhone': {
                        required:'Please provide contact number ',
                        phoneUS : 'Please enter valid mobile number'
                    },
                    'eventOrgEmail' : {
                        required:'Please provide email id',
                        email : 'Please provide valid email'
                    },
                    'eventCost' : 'Please enter the cost ',
                    'eventAgendaStart' : {
                        required:'Please select start date and time of the agenda',
                        AgendaDateChecker: 'Please Enter valid Dates'
                    },
                    'eventAgendaEnd' : {
                        required:'Please select end date and time of the agenda',
                        AgendaDateChecker: 'Please Enter valid Dates'
                    },
                    'eventAgendaTitle' : 'Please specify the title of the agenda',
                    'eventAgendaSummary' : 'Please provide description of agenda',
                    'eventAgendaVenue' : {
                        required:'Please select venue for the agenda',
                        NotNull :'Please select venue for the agenda'
                    },
                    'radios2': 'Please Select',
                    'eventSharing': 'Please Select'*/
                }
            });
            jQuery.validator.addMethod("NotNull", function (value, element) {
                if (value != "? undefined:undefined ?") {
                    return true;
                }
                else
                    return false;
            });
            jQuery.validator.addMethod("eventDateChecker", function (value, element) {
                var today = moment();
                var startday1 = moment(jQuery("#example-datetimepicker2").val());
                var endday1 = moment(jQuery("#example-datetimepicker1").val());
                var diff1 = startday1.diff(today, 'days');
                var diff2 = endday1.diff(startday1, 'days');

                if (diff1 >= 0 && diff2 >= 0)
                    return true;
                else
                    return false;
            });

            jQuery.validator.addMethod("venueValidation", function (value, element) {
                element.preventDefault;
                var isSuccess = false;
                var startday1 = moment(jQuery("#example-datetimepicker2").val());
                var startdate = moment(startday1).format("MMM Do YY");
                var starttime = moment(startday1).format('LT');
                var val = jQuery("#eventLocation option:selected").text();
                $.ajax(
                    {
                        type: "get",
                        url: "events/eventByVenue/" + val + "/" + startdate + "/" + starttime,
                        success: function (data) {
                            if (data.val == 0) {
                                isSuccess = true;
                            }
                            return false;
                        },
                        async: false
                    });
                return isSuccess;
            }, 'Event Already Happens There');

            jQuery.validator.addMethod("AgendaDateChecker", function (value, element) {
                //var today=moment();
                var agendastartday1 = moment(jQuery("#eventAgendaStart").val());
                var agendaendday1 = moment(jQuery("#eventAgendaEnd").val());
                //var diff1=agendastartday1.diff(today,'days');
                var diff2 = agendaendday1.diff(agendastartday1, 'days');

                if (diff2 >= 0)
                    return true;
                else
                    return false;
            });

            jQuery.validator.addMethod('phoneUS', function (phone_number, element) {
                phone_number = phone_number.replace(/\s+/g, '');
                return this.optional(element) || phone_number.length > 9 &&
                    phone_number.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/);
            });
            if (jQuery('.js-validation-bootstrap').valid()) {
                return true;
            }
            return false;
        };
        $scope.addCoordinator = function () {
            $scope.event.coordinators.push({ role: "" });
        };
        $scope.removeCoordinator = function (index) {
            $scope.event.coordinators.splice(index, 1);
        };
        //Suggestion for Coordinators
        $scope.loadSuggestion = function (value) {
            eventService.getStudents(value, function (err, response) {
                if (!err) {
                    $scope.students = response;
                }
            });
        };
        $scope.loadSuggestionFaculty = function (value) {
            eventService.getFaculty(value, function (err, response) {
                if (!err) {
                    $scope.faculties = response;
                }
            });
        };
        $scope.selected = {};

        $scope.createEvent = function (event) {
            // if(initValidationEventcreate())
            {
                console.log("valid");
                event.fdesc = jQuery("#eventDesc").summernote('code');
                event.startday = moment(event.start).format('D');
                event.startdate = moment(event.start).format("MMM Do YY");
                event.enddate = moment(event.end).format("MMM Do YY");
                event.startmonth = moment(event.start).format('MMMM');
                event.starttime = moment(event.start).format('LT');
                event.endtime = moment(event.end).format('LT');
                if ($scope.picDets) {
                    event.displayPicture = $scope.picDets;
                    event.imgsrc = "events/displayPicture/" + $scope.picDets.contentType + "/" + $scope.picDets.id + "/" + $scope.picDets.originalname;
                }
                event.commentscount = 0;
                event.likecount = "22";
                event.comment = [];
                event.reason="";
                event.agendaList = $scope.agendalist;
                event.dayVisedetails = eventService.orderAgenda(event);
                event.postTime = moment().format("MMM Do YY");
                event.status = "pending";
                event.winners=[];
                event.createdBy = $scope.Auth.tokenParsed.name;
                event.imageGallery=[];
                jQuery("#message-div").show();
                jQuery("#message-div").addClass("alert alert-success alert-white rounded");
                jQuery("#message-div").html('<button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button><div class="icon"><i class="fa fa-check"></i></div><strong>Success!</strong> Event has been saved successfully!');
                eventService.createEvent(JSON.parse(angular.toJson(event)), function (err, response) {
                    if (!err) {
                        jQuery(window).scrollTop(jQuery('#message-div').offset().top);
                        jQuery("#message-div").focus();
                        jQuery("#message-div").show();
                        jQuery("#message-div").addClass("alert alert-success alert-white rounded");
                        jQuery("#message-div").html('<button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button><div class="icon"><i class="fa fa-check"></i></div><strong>Success!</strong> Event has been saved successfully!');
                    }
                    else {
                        jQuery("#message-div").show();
                        jQuery("#message-div").addClass("alert alert-danger alert-white rounded");
                        jQuery("#message-div").html('<button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button><div class="icon"><i class="fa fa-check"></i></div><strong>Error!</strong>' + err);
                    }
                });
            }
        };
        $scope.clearDetails = function () {
            // var attatchments = $scope.gallPicDets.slice();
            var attatchments=[];
            attatchments.push($scope.picDets);
            gridfsService.removeDirtyAttachments(attatchments, function (err, response) {
                if (!err) {
                    console.log(response);
                    $scope.event = {};
                    $scope.event.coordinators = [{ role: "" }];
                    $scope.picDets = {};
                }
                else {
                    console.log(err);
                }
            });

        };
    }
})();
(function () {
    'use strict';
    var module = angular.module('app');

    module.controller('EventReportCtrl', EventReportCtrl);
    EventReportCtrl.$inject = ['$scope', '$rootScope', '$stateParams', '$state', '$compile', 'eventService'];

    function EventReportCtrl($scope, $rootScope, $stateParams, $state, $compile, eventService) {
        $scope.chartX = [];
        $scope.chartY = [];
        $scope.clubChartX = [];
        $scope.clubChartY = [];
        $scope.categoryChartX = [];
        $scope.categoryChartY = [];
        eventService.getNestedDistinctValues('orgBy', 'orgBody', 'associations', function (err, result) {

            if (!err) {
                $scope.associations = result;
                for (var i in result) {
                    eventService.getEventsByOrgBody('orgBy', result[i].name, function (err, response) {
                        if (!err) {
                            $scope.chartX.push(response[0].orgBy.name);
                            var valadds =
                                {
                                    "values": [],
                                    "line-color": "#D37E04",
                                    "line-width": "2px",
                                    "shadow": 0,
                                    "text": "Association",
                                    "marker": {
                                        "background-color": "#fff",
                                        "size": 4,
                                        "border-width": 2,
                                        "border-color": "#D37E04",
                                        "shadow": 0
                                    },
                                    "palette": 0
                                    // "visible": 1
                                };
                            valadds.text = response[0].orgBy.name;
                            valadds.values[0] = response.length;
                            $scope.chartY.push(valadds);
                            console.log($scope.chartY);
                        }
                    });
                }
                initChartVals();
            }
        });
        eventService.getNestedDistinctValues('orgBy', 'orgBody', 'clubs', function (err, result) {
           
            if (!err) {
                $scope.clubs = result;
                for (var i in result) {
                    eventService.getEventsByOrgBody('orgBy', result[i].name, function (err, response) {
                        if (!err) {
                            $scope.clubChartX.push(response[0].orgBy.name);
                            var valadds =
                            {
                                "values": [],
                                "line-color": "#D37E04",
                                "line-width": "2px",
                                "shadow": 0,
                                "text": "Club",
                                "marker": {
                                    "background-color": "#fff",
                                    "size": 4,
                                    "border-width": 2,
                                    "border-color": "#D37E04",
                                    "shadow": 0
                                },
                                "palette": 0
                                // "visible": 1
                            };
                            valadds.values[0] = response.length;
                            valadds.text = response[0].orgBy.name;
                            $scope.clubChartY.push(valadds);
                        }
                    });
                }
                initClubChartVals();
            }
        });
        eventService.getDistinctValues('category',function(err,result){
            if (!err) {
                $scope.category = result;
                for (var i in result) {
                    eventService.getEventsByCategory('category', result[i], function (err, response) {
                        if (!err) {
                            $scope.categoryChartX.push(response[0]);
                            var valadds =
                            {
                                "values": [],
                                "line-color": "#D37E04",
                                "line-width": "2px",
                                "shadow": 0,
                                "text": "Category",
                                "marker": {
                                    "background-color": "#fff",
                                    "size": 4,
                                    "border-width": 2,
                                    "border-color": "#D37E04",
                                    "shadow": 0
                                },
                                "palette": 0
                                // "visible": 1
                            };
                            valadds.values[0] = response.length;
                            valadds.text = response[0].category;
                            $scope.categoryChartY.push(valadds);
                        }
                    });
                }
                initCategoryChartVals();
            }
        });
        function initCategoryChartVals() {
            $scope.categoryJson = {
                "type": "bar",
                "plot": {
                    "animation": {
                        "effect": "11",
                        "method": "3",
                        "sequence": "ANIMATION_BY_PLOT_AND_NODE",
                        "speed": 10
                    }
                },
                "plotarea": {
                    "margin": "25 25 25 25"
                },
                "scale-y": {
                    "line-color": "none",
                    "guide": {
                        "line-style": "solid",
                        "line-color": "#d2dae2",
                        "line-width": "1px",
                        "alpha": 0.5
                    },
                    "tick": {
                        "visible": true
                    },
                    "item": {
                        "font-color": "#8391a5",
                        "font-size": "10px",
                        "padding-right": "5px"
                    }
                },
                "scale-x": {
                    "values":["category"],
                    "line-color": "#d2dae2",
                    "line-width": "2px",
                    "tick": {
                        "line-color": "#d2dae2",
                        "line-width": "1px"
                    },
                    "guide": {
                        "visible": true
                    },
                    "item": {
                        "font-color": "#8391a5",
                        "font-size": "10px",
                        "padding-top": "5px"
                    }
                },
                "legend": {
                    "layout": "x4",
                    "background-color": "none",
                    "shadow": 0,
                    "margin": "15 auto auto 15",
                    "border-width": 0,
                    "item": {
                        "font-color": "#707d94",
                        "padding": "0px",
                        "margin": "0px",
                        "font-size": "11px"
                    },
                    "marker": {
                        "show-line": "true",
                        "type": "match",
                        "font-family": "Arial",
                        "font-size": "10px",
                        "size": 4,
                        "line-width": 2,
                        "padding": "3px"
                    }
                },
                "crosshair-x": {
                    "lineWidth": 1,
                    "line-color": "#707d94",
                    "plotLabel": {
                        "shadow": false,
                        "font-color": "#000",
                        "font-family": "Arial",
                        "font-size": "10px",
                        "padding": "5px 10px",
                        "border-radius": "5px",
                        "alpha": 1
                    },
                    "scale-label": {
                        "font-color": "#ffffff",
                        "background-color": "#707d94",
                        "font-family": "Arial",
                        "font-size": "10px",
                        "padding": "5px 10px",
                        "border-radius": "5px"
                    }
                },
                "tooltip": {
                    "visible": false
                },
                "series": $scope.categoryChartY
            };
        }
        function initClubChartVals() {
            $scope.clubJson = {
                "type": "bar",
                "plot": {
                    "animation": {
                        "effect": "11",
                        "method": "3",
                        "sequence": "ANIMATION_BY_PLOT_AND_NODE",
                        "speed": 10
                    }
                },
                "plotarea": {
                    "margin": "25 25 25 25"
                },
                "scale-y": {
                    "line-color": "none",
                    "guide": {
                        "line-style": "solid",
                        "line-color": "#d2dae2",
                        "line-width": "1px",
                        "alpha": 0.5
                    },
                    "tick": {
                        "visible": true
                    },
                    "item": {
                        "font-color": "#8391a5",
                        "font-size": "10px",
                        "padding-right": "5px"
                    }
                },
                "scale-x": {
                    "values": ["clubs"],
                    "line-color": "#d2dae2",
                    "line-width": "2px",
                    "tick": {
                        "line-color": "#d2dae2",
                        "line-width": "1px"
                    },
                    "guide": {
                        "visible": true
                    },
                    "item": {
                        "font-color": "#8391a5",
                        "font-size": "10px",
                        "padding-top": "5px"
                    }
                },
                "legend": {
                    "layout": "x4",
                    "background-color": "none",
                    "shadow": 0,
                    "margin": "15 auto auto 15",
                    "border-width": 0,
                    "item": {
                        "font-color": "#707d94",
                        "padding": "0px",
                        "margin": "0px",
                        "font-size": "11px"
                    },
                    "marker": {
                        "show-line": "true",
                        "type": "match",
                        "font-family": "Arial",
                        "font-size": "10px",
                        "size": 4,
                        "line-width": 2,
                        "padding": "3px"
                    }
                },
                "crosshair-x": {
                    "lineWidth": 1,
                    "line-color": "#707d94",
                    "plotLabel": {
                        "shadow": false,
                        "font-color": "#000",
                        "font-family": "Arial",
                        "font-size": "10px",
                        "padding": "5px 10px",
                        "border-radius": "5px",
                        "alpha": 1
                    },
                    "scale-label": {
                        "font-color": "#ffffff",
                        "background-color": "#707d94",
                        "font-family": "Arial",
                        "font-size": "10px",
                        "padding": "5px 10px",
                        "border-radius": "5px"
                    }
                },
                "tooltip": {
                    "visible": false
                },
                "series": $scope.clubChartY
            };
        }
        function initChartVals() {
            $scope.associationJson = {
                "type": "bar",
                "plot": {
                    "animation": {
                        "effect": "11",
                        "method": "3",
                        "sequence": "ANIMATION_BY_PLOT_AND_NODE",
                        "speed": 10
                    }
                },
                "plotarea": {
                    "margin": "25 25 25 25"
                },
                "scale-y": {
                    "line-color": "none",
                    "guide": {
                        "line-style": "solid",
                        "line-color": "#d2dae2",
                        "line-width": "1px",
                        "alpha": 0.5
                    },
                    "tick": {
                        "visible": true
                    },
                    "item": {
                        "font-color": "#8391a5",
                        "font-size": "10px",
                        "padding-right": "5px"
                    }
                },
                "scale-x": {
                    "values":["Associations"],
                    "line-color": "#d2dae2",
                    "line-width": "2px",
                    "tick": {
                        "line-color": "#d2dae2",
                        "line-width": "1px"
                    },
                    "guide": {
                        "visible": true
                    },
                    "item": {
                        "font-color": "#8391a5",
                        "font-size": "10px",
                        "padding-top": "5px"
                    }
                },
                "legend": {
                    "layout": "x4",
                    "background-color": "none",
                    "shadow": 0,
                    "margin": "15 auto auto 15",
                    "border-width": 0,
                    "item": {
                        "font-color": "#707d94",
                        "padding": "0px",
                        "margin": "0px",
                        "font-size": "11px"
                    },
                    "marker": {
                        "show-line": "true",
                        "type": "match",
                        "font-family": "Arial",
                        "font-size": "10px",
                        "size": 4,
                        "line-width": 2,
                        "padding": "3px"
                    }
                },
                "crosshair-x": {
                    "lineWidth": 1,
                    "line-color": "#707d94",
                    "plotLabel": {
                        "shadow": false,
                        "font-color": "#000",
                        "font-family": "Arial",
                        "font-size": "10px",
                        "padding": "5px 10px",
                        "border-radius": "5px",
                        "alpha": 1
                    },
                    "scale-label": {
                        "font-color": "#ffffff",
                        "background-color": "#707d94",
                        "font-family": "Arial",
                        "font-size": "10px",
                        "padding": "5px 10px",
                        "border-radius": "5px"
                    }
                },
                "tooltip": {
                    "visible": false
                },
                "series": $scope.chartY
            };
        }
    }
})();

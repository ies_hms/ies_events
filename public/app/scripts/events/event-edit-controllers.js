(function (){
    'use strict';
    var App = angular.module('app');
    App.controller('EventEditCtrl', eventEditController);
    eventEditController.$inject = ['$scope','$stateParams', '$localStorage','$window', 'eventService'];
    function eventEditController($scope,$stateParams,$localStorage, $window, eventService) {
        $scope.event={};
        eventService.getEventDetails($stateParams.id,function(err,result){
            if(!err){
                $scope.event=result.data;
               jQuery("#eventDesc").summernote("code", result.data.fdesc);
               eventService.getFilter($scope.event.orgBody, function (err, result) {
                if (!err) {
                    $scope.groups = result.values;
                }
                });
            }
        });
        
        $scope.loadGroups=function(details){
            eventService.getFilter(details, function (err, result) {
                if (!err) {
                    $scope.groups = result.values;
                }
            });
        };
        eventService.getFilter('clubs',function (err, result) {
            if (!err) {
                $scope.clubs = result.values;
            }
        });
        eventService.getFilter('associations',function (err, result) {
            if (!err) {
                $scope.associations = result.values;
            }
        });
        eventService.getFilter('trusts',function (err, result) {
            if (!err) {
                $scope.trusts = result.values;
            }
        });
        eventService.getFilter('category',function (err, result) {
            if (!err) {
                $scope.categories = result;
            }
        });
        eventService.getFilter('institutions',function (err, result) {
            if (!err) {
                $scope.institutions = result.values;
            }
        });
        eventService.getFilter('venues',function (err, result) {
            if (!err) {
                $scope.venues = result.values;
            }
        });
        $scope.displayPicture={
            dzOptions: {
                dictDefaultMessage: "Drop your the Event Display Pictures here",
                url: "/events/uploadPicture",
                method: "put",
                parallelUploads: 1,
                acceptedFiles: 'image/jpeg, images/jpg, image/png',
                autoProcessQueue: true,
                uploadMultiple: true,
                addRemoveLinks: true,
                maxFileSize: 5120,
                paramName: function () {
                    return "displayPicture";
                },
                maxFiles: 10,
                createImageThumbnails: true,
                params: "Values For Gallery",
                renameFile: function (file) {
                    file.upload.filename = file.name;
                }
            },
            dzCallbacks: {
                init: function () {
                    this.on("addedfile", function (file) {
                        alert("Added file.");
                    });
                },
                "sending": function (file, xhr, formData) {
                    
                },
                "addedfile": function (file) {
                    $scope.isDisplayPicture = true;
                    $scope.lastAddedFile = file;
                },
                "success": function (file, xhr) {
                    $scope.showDpLoader = false;
                    $scope.showUploadDP = false;
                    $scope.picDets=xhr.displayPicture;
                    $scope.event.displayPictureDetails = $scope.picDets;
                    $scope.event.imgsrc = "events/displayPicture/"+$scope.picDets.contentType+"/"+$scope.picDets.id+"/"+$scope.picDets.originalname;
                    //loadDisplayPicture({ originalname: file.name });
                },
                "removedfile": function (file) {
                    eventService.removeAttachedDisplayPicture($scope.picDets);
                    $scope.isDisplayPicture = false;
                }
            },
            dzMethods: {

            }
        };
        
        $scope.saveImageCaption = function(pictureDets,caption){
            var index = $scope.event.gallPicDets.indexOf(pictureDets)
            pictureDets.caption=caption;
            $scope.event.gallPicDets[index]=pictureDets;
        };
       
        
        $scope.galleryPicture={
            dzOptions: {
                dictDefaultMessage: "Drop your Event Pictures here",
                url: "/events/uploadPicture",
                method: "put",
                parallelUploads: 1,
                acceptedFiles: 'image/jpeg, images/jpg, image/png',
                autoProcessQueue: true,
                uploadMultiple: true,
                addRemoveLinks: true,
                maxFileSize: 5120,
                paramName: function () {
                    return "displayPicture";
                },
                maxFiles: 20,
                createImageThumbnails: true,
                params: $scope.studentRecord,
                renameFile: function (file) {
                    file.upload.filename = file.name;
                }
            },
            dzCallbacks: {
                init: function () {
                    this.on("addedfile", function (file) {
                        alert("Added file.");
                    });
                },
                "sending": function (file, xhr, formData) {
                },
                "addedfile": function (file) {
                    
                },
                "success": function (file, xhr) {
                    $scope.gallPic=xhr.displayPicture;
                    // $scope.event.gallPicDets.push(xhr.displayPicture);
                    
                    $scope.event.imageGallery.push($scope.gallPic);
                                        // jQuery("#modal-normal").modal();
                },
                "removedfile": function (file) {
                    var index=$scope.gallPicDets.indexOf(JSON.parse(angular.toJson($scope.gallPic)));
                    // $scope.gallPicDets.splice(index,1);
                    eventService.removeAttachedDisplayPicture($scope.gallPic);
                }
            },
            dzMethods: {

            }
        };
        $scope.accolidePicture={
            dzOptions: {
                dictDefaultMessage: "Drop Pictures here",
                url: "/events/uploadPicture",
                method: "put",
                parallelUploads: 1,
                acceptedFiles: 'image/jpeg, images/jpg, image/png',
                autoProcessQueue: true,
                uploadMultiple: true,
                addRemoveLinks: true,
                maxFileSize: 5120,
                paramName: function () {
                    return "displayPicture";
                },
                maxFiles: 20,
                createImageThumbnails: true,
                params: $scope.studentRecord,
                renameFile: function (file) {
                    file.upload.filename = file.name;
                }
            },
            dzCallbacks: {
                init: function () {
                    this.on("addedfile", function (file) {
                        alert("Added file.");
                    });
                },
                "sending": function (file, xhr, formData) {
                },
                "addedfile": function (file) {
                    
                },
                "success": function (file, xhr) {
                    $scope.accoPic=xhr.displayPicture;
                    $scope.event.winners.push($scope.accoPic);
                },
                "removedfile": function (file) {
                    var index=$scope.event.winners.indexOf(JSON.parse(angular.toJson($scope.accoPic)));
                    $scope.event.winners.splice(index,1);
                    eventService.removeAttachedDisplayPicture($scope.accoPic);
                }
            },
            dzMethods: {

            }
        };
        // $scope.addAgenda = function () {
        //     var newItemNo = $scope.event.agendaList.length + 1;
        //     $scope.event.agendaList.push({ "no": "a" + newItemNo, "start": "", "end": "", "title": "", "summary": "",guest: [{}], "venue": "" });
        // };
        $scope.agendaList = [];
        $scope.agenda={guests:[{}]};
        $scope.addAgenda = function (details) {
            details.day = moment(details.start).diff(moment(event.start), 'days')+1;
            details.startdate = moment(details.start).format('LL');
            details.starttime = moment(details.start).format('LT');
            details.endtime = moment(details.end).format('LT');
            $scope.event.agendaList.push(details);
            $scope.agenda={guests:[{}]};
        };
        $scope.addGuest = function (index) {
            $scope.agenda.guests.push({});
        }
        $scope.removeGuest = function ( currentIndex) {
            $scope.agenda.guests.splice(currentIndex, 1);
        }
        $scope.setupAgenda = function (index) {
            $scope.selectedIndex = index;
            $scope.agenda = $scope.event.agendaList[index];
        };
        $scope.editAgenda = function (details) {
            details.day = moment(details.start).diff(moment(event.start), 'days')+1;
            details.startdate = moment(details.start).format('LL');
            details.starttime = moment(details.start).format('LT');
            details.endtime = moment(details.end).format('LT');
            $scope.event.agendaList[$scope.selectedIndex].push(details);
        };
        $scope.removeAgenda = function (index) {
            $scope.event.agendaList.splice(index,1);
        }
        $scope.addCoordinator = function () {
            $scope.event.coordinators.push({ role: "" });
        };
        $scope.removeCoordinator = function (index) {
            $scope.event.coordinators.splice(index, 1);
        };
        $scope.addWinner = function(){
            $scope.event.winners.push({});
        };
        $scope.removeWinner = function(index){
            $scope.event.winners.splice(index, 1);
        }
        $scope.addSponsor = function(details){
            $scope.event.sponsors.push(details);
            $scope.sponsor={};
        }
        $scope.removeSponsor = function(index){
            $scope.event.sponsors.splice(index,1);
        }
        //Suggestion for Coordinators
        $scope.loadSuggestion = function (value) {
            eventService.getStudents(value, function (err, response) {
                if (!err) {
                    $scope.students = response;
                }
            });
        };
        $scope.loadSuggestionFaculty = function (value) {
            eventService.getFaculty(value, function (err, response) {
                if (!err) {
                    $scope.faculties = response;
                }
            });
        };
        $scope.selected = {}
        $scope.removeSelectedPic=function(details){
             eventService.removeAttachedDisplayPicture(details);
             $scope.event.displayPictureDetails = {};
             $scope.event.imgsrc = {};
        };
        $scope.removeSelectedGalleryPic=function(details){
            var index = $scope.event.imageGallery.indexOf(details);
            $scope.event.imageGallery.splice(index,1);
            eventService.removeAttachedDisplayPicture(details);
        };
        $scope.removeSelectedWinnerPic=function(details){
            var index=$scope.event.winners.indexOf(details);
            $scope.event.winners.splice(index,1);
            eventService.removeAttachedDisplayPicture(details);
        }
        $scope.editEvent = function (event) {
            $scope.event.status ="pending";
            $scope.event.startdate = moment(event.start).format("MMM Do YY");
            $scope.event.enddate = moment(event.end).format("MMM Do YY");
            $scope.event.startmonth = moment(event.start).format('MMMM');
            $scope.event.starttime = moment(event.start).format('LT');
            $scope.event.endtime = moment(event.end).format('LT');
            $scope.event.eventPostSummary = jQuery("#eventPostSummary").summernote('code');
            
            eventService.editEvent($scope.event._id,JSON.parse(angular.toJson(event)),function (err, response){
                if (!err) {
                    jQuery(window).scrollTop(jQuery('#message-div').offset().top);
                    jQuery("#message-div").focus();
                    jQuery("#message-div").show();
                    jQuery("#message-div").addClass("alert alert-success alert-white rounded");
                    jQuery("#message-div").html('<button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button><div class="icon"><i class="fa fa-check"></i></div><strong>Success!</strong> Event has been edited successfully!');
                }
                else {
                    jQuery("#message-div").show();
                    jQuery("#message-div").addClass("alert alert-danger alert-white rounded");
                    jQuery("#message-div").html('<button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button><div class="icon"><i class="fa fa-check"></i></div><strong>Error!</strong>' + err);
                }
            });
        };
    }
})();
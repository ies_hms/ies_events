(function () {
    'use strict';
    var App = angular.module('app');
    App.controller('EventClubSetupCtrl', eventClubSetupController);
    eventClubSetupController.$inject = ['$scope', '$localStorage', '$window', 'eventService'];
    function eventClubSetupController($scope, $localStorage, $window, eventService) {
        eventService.getFilter('clubs', function (err, result) {
            if (!err) {
                $scope.clubs = result.values;
            }
        });
        eventService.getFilter('club-categories', function (err, result) {
            if (!err) {
                $scope.clubCategories = result.values;
            }
        });
        $scope.createClub = function (details) {
            initValidationAssociation();
            if (jQuery('.js-validation-bootstrap').valid()) {
                $scope.clubs.push(details);
                eventService.addFilter('clubs', JSON.parse(angular.toJson($scope.clubs)));
                eventService.getFilter('clubs', function (err, result) {
                    if (!err) {
                        $scope.clubs = result.values;
                    }
                });
                jQuery('#createClubModal').modal('toggle');
                $scope.club={};
            }
        };
        $scope.setupClub = function (index) {
            $scope.selectedClubIndex = index;
            $scope.club = $scope.clubs[index];
        };
        $scope.editClub = function (details) {
            // initValidationAssociation();
            if (jQuery('.js-validation-bootstrap').valid()) {
                $scope.clubs[$scope.selectedClubIndex] = details;
                eventService.addFilter('clubs', JSON.parse(angular.toJson($scope.clubs)));
                
                jQuery('#editClubModal').modal('toggle');
            }
        };
        $scope.deleteClub = function () {
            $scope.clubs.splice($scope.selectedClubIndex, 1);
            eventService.addFilter('clubs', JSON.parse(angular.toJson($scope.clubs)));
        };
        var initValidationAssociation = function () {
            jQuery('.js-validation-bootstrap').validate({
                ignore: [],
                errorClass: 'help-block animated fadeInDown',
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    jQuery(e).parents('.form-group > div').append(error);
                },
                highlight: function (e) {
                    var elem = jQuery(e);
                    elem.closest('.form-group').removeClass('has-error').addClass('has-error');
                    elem.closest('.help-block').remove();
                },
                success: function (e) {
                    var elem = jQuery(e);

                    elem.closest('.form-group').removeClass('has-error');
                    elem.closest('.help-block').remove();
                },
                rules: {
                    'clubName': {
                        required: true,
                        unique: true
                    },
                    'clubAdmin': {
                        required: true
                    }
                },
                messages: {
                    'clubName': 'Please enter Admin Name',
                    'clubName': {
                        required: 'Please Enter Name',
                        unique: 'Club already present'
                    },
                }
            });
            jQuery.validator.addMethod("unique", function (value, element) {
                var clubs = JSON.parse(angular.toJson($scope.clubs));
                function findClub(element) {
                    return element.name == value;
                }
                var index = clubs.findIndex(findClub);
                if (index == -1) {
                    return true;
                }
                else
                    return false;
            });
        };
    }
})();
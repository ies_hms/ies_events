(function () {
    'use strict';
    var App = angular.module('app');
    App.controller('EventVenueSetupCtrl', eventVenueSetupController);
    eventVenueSetupController.$inject = ['$scope', '$localStorage', '$window', 'eventService'];
    function eventVenueSetupController($scope, $localStorage, $window, eventService) {
        eventService.getFilter('venues', function (err, result) {
            if (!err) {
                if (result.values)
                    $scope.venues = result.values;
            }
        });
        $scope.createVenue = function (details) {
                initValidationVenue();
                if (jQuery('.js-validation-bootstrap').valid()) {
                    $scope.venues.push(details);
                    eventService.addFilter('venues', JSON.parse(angular.toJson($scope.venues)));
                    jQuery('#createVenueModal').modal('toggle');
                    $scope.venue = {};
                }
        };
        $scope.addorCreateVenue = function(){
            if ($scope.venues) {
            }
            else{
                eventService.createFilter('venues',function(err,response){});
            }
        }
        $scope.setupVenue = function (index) {
            $scope.selectedIndex = index;
            $scope.venue = $scope.venues[index];
        };
        $scope.editVenue = function (details) {
            // initValidationVenue();
            if (jQuery('.js-validation-bootstrap').valid()) {
                $scope.venues[$scope.selectedIndex] = details;
                eventService.addFilter('venues', JSON.parse(angular.toJson($scope.venues)));
                jQuery('#editVenueModal').modal('toggle');
            }
        };
        $scope.deleteVenue = function () {
            $scope.venues.splice($scope.selectedIndex, 1);
            eventService.addFilter('venues', JSON.parse(angular.toJson($scope.venues)));

        };
        var initValidationVenue = function () {
            jQuery('.js-validation-bootstrap').validate({
                ignore: [],
                errorClass: 'help-block animated fadeInDown',
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    jQuery(e).parents('.form-group > div').append(error);
                },
                highlight: function (e) {
                    var elem = jQuery(e);
                    elem.closest('.form-group').removeClass('has-error').addClass('has-error');
                    elem.closest('.help-block').remove();
                },
                success: function (e) {
                    var elem = jQuery(e);

                    elem.closest('.form-group').removeClass('has-error');
                    elem.closest('.help-block').remove();
                },
                rules: {
                    'venueName': {
                        required: true,
                        unique: true
                    },
                    'venueUrl': {
                        url: true
                    }
                },
                messages: {
                    'venueName': {
                        required: 'Please Enter Name',
                        unique: 'venue already present'
                    },
                    'venueUrl': {
                        url: 'Enter valid location'
                    }
                }
            });
            jQuery.validator.addMethod("unique", function (value, element) {
                var venues = JSON.parse(angular.toJson($scope.venues));
                function findVenue(element) {
                    return element.name == value;
                }
                var index = venues.findIndex(findVenue);
                if (index == -1) {
                    return true;
                }
                else
                    return false;
            });
        };

    }
})();
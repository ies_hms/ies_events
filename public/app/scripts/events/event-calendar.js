(function () {
    'use strict';
    var module = angular.module('app');

    module.controller('EventCalendarCtrl', EventCalendarCtrl);
    EventCalendarCtrl.$inject = ['$scope', '$rootScope', '$stateParams', '$state', '$compile', 'eventService'];

    function EventCalendarCtrl($scope, $rootScope, $stateParams, $state, $compile, eventService) {
        $scope.associations = [];
        $scope.clubs = [];
        $scope.institutions = [];
        $scope.trusts = [];
        $scope.events = [];
        $scope.datefilters = ["Today", "Tomorrow", "This Week", "This Weekend", "This Month", "Next Month"];

        eventService.getAllEvents(function (err, result) {
            if (!err) {
                for (var i in result) {
                    if ($scope.dateFilter($scope.categoryFilter(result[i]) && $scope.venueFilter(result[i])) && $scope.dateFilter(result[i])) {
                        $scope.events.push({ title: result[i].title, start: new Date(result[i].start), end: new Date(result[i].end), url: "#!/events/" + result[i]._id });
                    }
                    else {
                        $scope.events = [];
                    }
                }
                initCalendar();
            }
        });
        eventService.getDistinctValues('category', function (err, result) {
            if (!err) {
                $scope.categories = result;
            }
        });
        eventService.getDistinctValues('venue', function (err, result) {
            if (!err) {
                $scope.venues = result;
            }
        });
        eventService.getNestedDistinctValues('orgBy','orgBody','associations',function(err,result){
            if (!err) {
                $scope.associations = result;
            }
        });
        eventService.getNestedDistinctValues('orgBy','orgBody','clubs',function(err,result){
            if (!err) {
                $scope.clubs = result;
            }
        });
        eventService.getNestedDistinctValues('orgBy','orgBody','institutions',function(err,result){
            if (!err) {
                $scope.institutions = result;
            }
        });


        var initCalendar = function () {
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            jQuery('.js-calendar').fullCalendar({
                firstDay: 1,
                editable: true,
                droppable: true,
                defaultView: 'agendaWeek',
                editable: true,
                minTime: "09:00:00",
                maxTime: "19:00:00",
                header: {
                    left: 'title',
                    right: 'prev,next month,agendaWeek,agendaDay'
                },
                drop: function (date, allDay) { // this function is called when something is dropped
                    // retrieve the dropped element's stored Event Object
                    var originalEventObject = jQuery(this).data('eventObject');

                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = jQuery.extend({}, originalEventObject);

                    // assign it the date that was reported
                    copiedEventObject.start = date;

                    // render the event on the calendar
                    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    jQuery('.js-calendar').fullCalendar('renderEvent', copiedEventObject, true);

                    // remove the element from the "Draggable Events" list
                    jQuery(this).remove();
                },


                events: $scope.events,

                eventClick: function (event, jsEvent, view) {
                    // //set the values and open the modal
                    // $('#modalTitle').html(event.title);
                    // $('#modalBody').html(event.description);
                    // $('#fullCalModal').modal();
                }
            });
        };
        $scope.eventIncludes = [];
        $scope.venueIncludes = [];
        $scope.dateIncludes = [];
        $scope.institutionIncludes = [];
        $scope.associationIncludes = [];
        $scope.clubIncludes = [];
        $scope.trustIncludes = [];
        $scope.monthIncludes = [];
        $scope.weekIncludes = [];
        $scope.weekEndIncludes = [];
        $scope.includeDate = function (date) {
            if (date == "Today") {
                var current_date = moment().format("DD/MM/YYYY");
                var i = $.inArray(current_date, $scope.dateIncludes);
                if (i > -1) {
                    $scope.dateIncludes.splice(i, 1);
                } else {
                    $scope.dateIncludes.push(current_date);
                }
            }
            if (date == "Tomorrow") {
                var tommorrow_date = moment().add(1, 'days').format("DD/MM/YYYY");
                var i = $.inArray(tommorrow_date, $scope.dateIncludes);
                if (i > -1) {
                    $scope.dateIncludes.splice(i, 1);
                } else {
                    $scope.dateIncludes.push(tommorrow_date);
                }
            }
            if (date == "This Month") {
                var thismonth = moment().month();
                var i = $.inArray(thismonth, $scope.monthIncludes);
                if (i > -1) {
                    $scope.monthIncludes.splice(i, 1);
                } else {
                    $scope.monthIncludes.push(thismonth);
                }
            }
            if (date == "Next Month") {
                var nextmonth = moment().add(1, 'months').month();
                var i = $.inArray(nextmonth, $scope.monthIncludes);
                if (i > -1) {
                    $scope.monthIncludes.splice(i, 1);
                } else {
                    $scope.monthIncludes.push(nextmonth);
                }
            }
            if (date == "This Week") {
                var weeknumber = moment().week();
                var i = $.inArray(weeknumber, $scope.weekIncludes);
                if (i > -1) {
                    $scope.weekIncludes.splice(i, 1);
                } else {
                    $scope.weekIncludes.push(weeknumber);
                }
            }
            if (date == "This Weekend") {
                var weekno = 7 - moment().day();
                var weekEndDay = moment().add(weekno, 'days').format("DD/MM/YYYY");
                var i = $.inArray(weekEndDay, $scope.weekEndIncludes);
                if (i > -1) {
                    $scope.weekEndIncludes.splice(i, 1);
                } else {
                    $scope.weekEndIncludes.push(weekEndDay);
                }
            }
        }
        $scope.includeEvent = function (event) {
            var i = $.inArray(event, $scope.eventIncludes);
            if (i > -1) {
                $scope.eventIncludes.splice(i, 1);
            } else {
                $scope.eventIncludes.push(event);
            }
        }
        $scope.includeVenue = function (venue) {
            var i = $.inArray(venue.name, $scope.venueIncludes);
            if (i > -1) {
                $scope.venueIncludes.splice(i, 1);
            } else {
                $scope.venueIncludes.push(venue.name);
            }
        }
        $scope.includeAssociation = function (association) {
            var i = $.inArray(association, $scope.associationIncludes);
            if (i > -1) {
                $scope.associationIncludes.splice(i, 1);
            } else {
                $scope.associationIncludes.push(association);
            }
        }
        $scope.includeClub = function (club) {
            var i = $.inArray(club, $scope.clubIncludes);
            if (i > -1) {
                $scope.clubIncludes.splice(i, 1);
            } else {
                $scope.clubIncludes.push(club);
            }
        }
        $scope.includeInstitution = function (institution) {
            var i = $.inArray(institution, $scope.institutionIncludes);
            if (i > -1) {
                $scope.institutionIncludes.splice(i, 1);
            } else {
                $scope.institutionIncludes.push(institution);
            }
        }
        $scope.includeTrust = function (trust) {
            var i = $.inArray(trust, $scope.trustIncludes);
            if (i > -1) {
                $scope.trustIncludes.splice(i, 1);
            } else {
                $scope.trustIncludes.push(trust);
            }
        }
        $scope.categoryFilter = function (event) {
            if ($scope.eventIncludes.length > 0) {
                if ($.inArray(event.category, $scope.eventIncludes) < 0)
                    return;
            }
            return true;
        }
        $scope.venueFilter = function (event) {
            if ($scope.venueIncludes.length > 0) {
                if ($.inArray(event.venue.name, $scope.venueIncludes) < 0)
                    return;
            }
            return true;
        }
        $scope.associationFilter = function (event) {
            if ($scope.associationIncludes.length > 0) {
                if ($.inArray(event.orgBy.name, $scope.associationIncludes) < 0)
                    return;
            }
            return true;
        }
        $scope.clubFilter = function (event) {
            if ($scope.clubIncludes.length > 0) {
                if ($.inArray(event.orgBy.name, $scope.clubIncludes) < 0)
                    return;
            }
            return true;
        }
        $scope.trustFilter = function (event) {
            if ($scope.trustIncludes.length > 0) {
                if ($.inArray(event.orgBy.name, $scope.trustIncludes) < 0)
                    return;
            }
            return true;
        }
        $scope.institutionFilter = function (event) {
            if ($scope.institutionIncludes.length > 0) {
                if ($.inArray(event.orgBy.name, $scope.institutionIncludes) < 0)
                    return;
            }
            return true;
        }
        $scope.dateFilter = function (event) {
            if ($scope.dateIncludes.length > 0) {
                var event_date = moment(event.start).format("DD/MM/YYYY");
                if ($.inArray(event_date, $scope.dateIncludes) < 0)
                    return;
            }
            if ($scope.monthIncludes.length > 0) {
                var event_month = moment(event.start).month();
                if ($.inArray(event_month, $scope.monthIncludes) < 0)
                    return;
            }
            if ($scope.weekIncludes.length > 0) {
                var event_week = moment(event.start).week();
                if ($.inArray(event_week, $scope.weekIncludes) < 0)
                    return;
            }
            if ($scope.weekEndIncludes.length > 0) {
                var event_date = moment(event.start).format("DD/MM/YYYY");
                if ($.inArray(event_date, $scope.weekEndIncludes) < 0)
                    return;
            }
            return true;
        }

    }

})();

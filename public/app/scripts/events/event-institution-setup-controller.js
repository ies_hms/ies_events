(function () {
    'use strict';
    var App = angular.module('app');
    App.controller('EventInstitutionSetupCtrl', eventInstitutionSetupController);
    eventInstitutionSetupController.$inject = ['$scope', '$localStorage', '$window', 'eventService'];
    function eventInstitutionSetupController($scope, $localStorage, $window, eventService) {
        eventService.getFilter('institutions', function (err, result) {
            if (!err) {
                $scope.institutions = result.values;
            }
        });
        eventService.getFilter('institution-categories', function (err, result) {
            if (!err) {
                $scope.institutionCategories = result.values;
            }
        });
        $scope.createInstitution = function (details) {
            initValidationInstitution();
            if (jQuery('.js-validation-bootstrap').valid()) {
                $scope.institutions.push(details);
                eventService.addFilter('institutions', JSON.parse(angular.toJson($scope.institutions)));
                jQuery('#createInstitutionModal').modal('toggle');
                $scope.institution={};
            }
        };
        $scope.setupInstitution = function (index) {
            $scope.selectedIndex = index;
            $scope.institution = $scope.institutions[index];
        };
        $scope.editInstitution = function (details) {
            // initValidationInstitution();
            if (jQuery('.js-validation-bootstrap').valid()) {
                $scope.institutions[$scope.selectedIndex] = details;
                eventService.addFilter('institutions', JSON.parse(angular.toJson($scope.institutions)));
                jQuery('#editInstitutionModal').modal('toggle');
            }
        };
        $scope.deleteInstitution = function () {
            $scope.institutions.splice($scope.selectedIndex, 1);
            eventService.addFilter('institutions', JSON.parse(angular.toJson($scope.institutions)));
            
        };
        var initValidationInstitution = function () {
            jQuery('.js-validation-bootstrap').validate({
                ignore: [],
                errorClass: 'help-block animated fadeInDown',
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    jQuery(e).parents('.form-group > div').append(error);
                },
                highlight: function (e) {
                    var elem = jQuery(e);
                    elem.closest('.form-group').removeClass('has-error').addClass('has-error');
                    elem.closest('.help-block').remove();
                },
                success: function (e) {
                    var elem = jQuery(e);

                    elem.closest('.form-group').removeClass('has-error');
                    elem.closest('.help-block').remove();
                },
                rules: {
                    'institutionName': {
                        required: true,
                        unique: true
                    },
                    'institutionAdmin': {
                        required: true
                    }
                },
                messages: {
                    'institutionAdmin': 'Please enter Admin Name',
                    'institutionName': {
                        required: 'Please Enter Name',
                        unique: 'Institution already present'
                    },
                }
            });
            jQuery.validator.addMethod("unique", function (value, element) {
                var institutions = JSON.parse(angular.toJson($scope.institutions));
                function findInstitution(element) {
                    return element.name == value;
                }
                var index = institutions.findIndex(findInstitution);
                if (index == -1) {
                    return true;
                }
                else
                    return false;
            });
        };
        
    }
})();
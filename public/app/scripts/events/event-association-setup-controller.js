(function () {
    'use strict';
    var App = angular.module('app');
    App.controller('EventAssociationSetupCtrl', eventAssociationSetupController);
    eventAssociationSetupController.$inject = ['$scope', '$localStorage', '$window', 'eventService'];
    function eventAssociationSetupController($scope, $localStorage, $window, eventService) {
        eventService.getFilter('associations', function (err, result) {
            if (!err) {
                $scope.associations = result.values;
            }
        });
        eventService.getFilter('association-categories', function (err, result) {
            if (!err) {
                $scope.associationCategories = result.values;
            }
        });
        $scope.createAssociation = function (details) {
            initValidationAssociation();
            if (jQuery('.js-validation-bootstrap').valid()) {
                $scope.associations.push(details);
                eventService.addFilter('associations', JSON.parse(angular.toJson($scope.associations)));
                jQuery('#createAssociationModal').modal('toggle');
                $scope.association={};
            }
        };
        $scope.setupAssociation = function (index) {
            $scope.selectedIndex = index;
            $scope.association = $scope.associations[index];
        };
        $scope.editAssociation = function (details) {
            // initValidationAssociation();
            if (jQuery('.js-validation-bootstrap').valid()) {
                $scope.associations[$scope.selectedIndex] = details;
                eventService.addFilter('associations', JSON.parse(angular.toJson($scope.associations)));
                jQuery('#editAssociationModal').modal('toggle');
            }
        };
        $scope.deleteAssociation = function () {
            $scope.associations.splice($scope.selectedIndex, 1);
            eventService.addFilter('associations', JSON.parse(angular.toJson($scope.associations)));
            
        };
        var initValidationAssociation = function () {
            jQuery('.js-validation-bootstrap').validate({
                ignore: [],
                errorClass: 'help-block animated fadeInDown',
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    jQuery(e).parents('.form-group > div').append(error);
                },
                highlight: function (e) {
                    var elem = jQuery(e);
                    elem.closest('.form-group').removeClass('has-error').addClass('has-error');
                    elem.closest('.help-block').remove();
                },
                success: function (e) {
                    var elem = jQuery(e);

                    elem.closest('.form-group').removeClass('has-error');
                    elem.closest('.help-block').remove();
                },
                rules: {
                    'associationName': {
                        required: true,
                        unique: true
                    },
                    'associationAdmin': {
                        required: true
                    }
                },
                messages: {
                    'associationAdmin': 'Please enter Admin Name',
                    'associationName': {
                        required: 'Please Enter Name',
                        unique: 'Association already present'
                    },
                }
            });
            jQuery.validator.addMethod("unique", function (value, element) {
                var associations = JSON.parse(angular.toJson($scope.associations));
                function findAssociation(element) {
                    return element.name == value;
                }
                var index = associations.findIndex(findAssociation);
                if (index == -1) {
                    return true;
                }
                else
                    return false;
            });
        };
        
    }
})();
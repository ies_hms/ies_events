(function () {
    'use strict';
    var App = angular.module('app');
    App.controller('EventListViewCtrl', eventsListViewController);
    eventsListViewController.$inject = ['$scope', '$localStorage', '$window', 'eventService'];
    function eventsListViewController($scope, $localStorage, $window, eventService) {
        
        eventService.getAllEvents(function (err, result) {
            if (!err) {
                $scope.events = result;
            }
        });
        $scope.chosenValue = "All";
        $scope.myFilter = function(event){
            if(event.status == $scope.chosenValue){
                return event;
            }
            else if($scope.chosenValue == "All"){
                return event;
            }
        }
        $scope.setUp = function(index){
            $scope.selectedIndex = index;
        };
        $scope.approveEvent = function (index) {
            $scope.events[index].status="Approved";
            eventService.editEvent($scope.events[index]._id,JSON.parse(angular.toJson($scope.events[index])),function(err,res){
                if(!err){
                    console.log(res);
                }
            });
        };
        $scope.rejectEvent =function(reason1){
            $scope.events[$scope.selectedIndex].status = "Rejected";
            $scope.events[$scope.selectedIndex].reason = reason1;
            eventService.editEvent($scope.events[$scope.selectedIndex]._id,JSON.parse(angular.toJson($scope.events[$scope.selectedIndex])),function(err,res){
                if(!err){
                    console.log(res);
                }
            });
        };
    }
})();
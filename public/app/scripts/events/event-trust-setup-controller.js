(function () {
    'use strict';
    var App = angular.module('app');
    App.controller('EventTrustSetupCtrl', eventTrustSetupController);
    eventTrustSetupController.$inject = ['$scope', '$localStorage', '$window', 'eventService'];
    function eventTrustSetupController($scope, $localStorage, $window, eventService) {
        eventService.getFilter('trusts', function (err, result) {
            if (!err) {
                $scope.trusts = result.values;
            }
        });
        eventService.getFilter('trust-categories', function (err, result) {
            if (!err) {
                $scope.trustCategories = result.values;
            }
        });
        $scope.createTrust = function (details) {
            initValidationTrust();
            if (jQuery('.js-validation-bootstrap').valid()) {
                $scope.trusts.push(details);
                eventService.addFilter('trusts', JSON.parse(angular.toJson($scope.trusts)));
                jQuery('#createTrustModal').modal('toggle');
                $scope.trust={};
            }
        };
        $scope.setupTrust = function (index) {
            $scope.selectedIndex = index;
            $scope.trust = $scope.trusts[index];
        };
        $scope.editTrust = function (details) {
            // initValidationTrust();
            if (jQuery('.js-validation-bootstrap').valid()) {
                $scope.trusts[$scope.selectedIndex] = details;
                eventService.addFilter('trusts', JSON.parse(angular.toJson($scope.trusts)));
                jQuery('#editTrustModal').modal('toggle');
            }
        };
        $scope.deleteTrust = function () {
            $scope.trusts.splice($scope.selectedIndex, 1);
            eventService.addFilter('trusts', JSON.parse(angular.toJson($scope.trusts)));
            
        };
        var initValidationTrust = function () {
            jQuery('.js-validation-bootstrap').validate({
                ignore: [],
                errorClass: 'help-block animated fadeInDown',
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    jQuery(e).parents('.form-group > div').append(error);
                },
                highlight: function (e) {
                    var elem = jQuery(e);
                    elem.closest('.form-group').removeClass('has-error').addClass('has-error');
                    elem.closest('.help-block').remove();
                },
                success: function (e) {
                    var elem = jQuery(e);

                    elem.closest('.form-group').removeClass('has-error');
                    elem.closest('.help-block').remove();
                },
                rules: {
                    'trustName': {
                        required: true,
                        unique: true
                    },
                    'trustAdmin': {
                        required: true
                    }
                },
                messages: {
                    'trustAdmin': 'Please enter Admin Name',
                    'trustName': {
                        required: 'Please Enter Name',
                        unique: 'Trust already present'
                    },
                }
            });
            jQuery.validator.addMethod("unique", function (value, element) {
                var trusts = JSON.parse(angular.toJson($scope.trusts));
                function findTrust(element) {
                    return element.name == value;
                }
                var index = trusts.findIndex(findTrust);
                if (index == -1) {
                    return true;
                }
                else
                    return false;
            });
        };
        
    }
})();
var keycloakConfig = {
    "realm": "ies",
    "auth-server-url": "http://adm.psgcas.ac.in:8080/auth",
    "url": "http://adm.psgcas.ac.in:8080/auth",
    "ssl-required": "external",
    "resource": "ies_events",
    "clientId": "ies_events",
    "public-client": true
};

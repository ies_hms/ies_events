var keycloakConfig = {
    "realm": "ies",
    "auth-server-url": "http://localhost:8080/auth",
    "url": "http://localhost:8080/auth",
    "ssl-required": "external",
    "resource": "ies_events",
    "clientId": "ies_events",
    "public-client": true
};
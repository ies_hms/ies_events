var eventFilterDao = require('../daos/eventFilterDao');
var appLogger = require('../logging/appLogger');

var config = require('../config/config.' + process.env.NODE_ENV);
var emailConfig = config.emailConfig;
function getAllFilterlist(callback) {
    eventFilterDao.getAll(callback);
}
function getFilter(query, callback) {
    eventFilterDao.getByQuery(query, callback);
}
function updateFilterDetails(query, details, callback) {
    eventFilterDao.updateMany(query, details, callback);
}
function createFilter(details, callback) {
    eventFilterDao.create(details, callback);
}
module.exports.getAllFilterlist = getAllFilterlist;
module.exports.updateFilterDetails = updateFilterDetails;
module.exports.getFilter = getFilter;
module.exports.createFilter = createFilter;
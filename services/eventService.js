var eventDao = require('../daos/eventDao');
var studentsDao = require('../daos/studentsDao');
var eventFilterDao = require('../daos/eventFilterDao');
var baseDao = require('../daos/baseDao');
var appLogger = require('../logging/appLogger');
var mailService = require('./mailService');
var smsService = require('./smsService');
var gridfsDao = require('../daos/gridfsDao');
var mongodb = require('mongodb');
var imageDao = require('../daos/imageDao');
var facultiesDao = require('../daos/facultiesDao');

var config = require('../config/config.' + process.env.NODE_ENV);
var emailConfig = config.emailConfig;

function getAllEventlist(callback) {
    eventDao.getAll(callback);
}

function removeEvent(enumber, callback) {
    eventDao.remove(enumber, callback);
}

function createEvent(eventDetails, callback) {
    eventDao.create(eventDetails, callback);
}

function postComment(id, commentDetails, callback) {
    var id = new mongodb.ObjectID(id);
    var detailstoUpdate = {};
    detailstoUpdate.comment = commentDetails;
    eventDao.updateMany({ _id: id }, detailstoUpdate, callback);
}

function editEvent(id, eventDetails, callback) {
    // var id = new mongodb.ObjectID(id);
    eventDao.updateById(id, eventDetails, callback);
}

function getEventsByQuery(query, callback) {
    eventDao.getByQuery(query, callback);
}
function getEventById(id, callback) {
    var id = new mongodb.ObjectID(id);
    eventDao.getById({ _id: id }, callback);
}

function getDistinctValuesFromEvents(keyName, query, callback) {
    eventDao.getDistinctValues(keyName, query, callback)
}

function uploadDisplayPicture(displayPicture, callback) {
    imageDao.create(displayPicture, callback);
}
function deletePictureinSavedImages(displayPicture, callback) {
    var query = {};
    query["displayPicture.id"] = displayPicture.id;
    imageDao.removeByQuery(query, callback);
}
function deletePictureinFile(displayPicture, callback) {
    gridfsDao.dropAttachment(displayPicture.id, callback);
}


function getStudentDetails(searchParam, callback) {
    var query = { name: { $regex: searchParam, $options: "i" } };
    studentsDao.getByQuery(query, callback);
}
function removeDirtyAttachments(files, callback) {
    var processedFiles = [];
    files.forEach(function (file) {
        gridfsDao.dropAttachment(file.id, function (attachErr, attachRes) {
            if (!attachErr) {
                processedFiles.push(file);
                var query = {};
                query["displayPicture.id"] = file.id;
                imageDao.removeByQuery(query,function(err,response){
                    if(err){
                        console.log(err);
                    }
                });
                if (processedFiles.length == files.length) {
                    callback(null, processedFiles);
                }
            }
        });
    })
}
function getFacultyDetails(searchParam,callback){
    var query={name:{$regex:searchParam,$options:"i"}};
    facultiesDao.getByQuery(query,callback);
}
module.exports.createEvent = createEvent;
module.exports.editEvent = editEvent;
module.exports.postComment = postComment;
module.exports.getAllEventlist = getAllEventlist;
module.exports.getEventById = getEventById;
module.exports.getDistinctValuesFromEvents = getDistinctValuesFromEvents;
module.exports.removeEvent = removeEvent;
module.exports.uploadDisplayPicture = uploadDisplayPicture;
module.exports.deletePictureinSavedImages = deletePictureinSavedImages;
module.exports.deletePictureinFile = deletePictureinFile;
module.exports.getEventsByQuery = getEventsByQuery;
module.exports.getStudentDetails = getStudentDetails;
module.exports.getFacultyDetails = getFacultyDetails;
module.exports.removeDirtyAttachments = removeDirtyAttachments;

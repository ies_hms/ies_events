var express = require('express');
var router = express.Router();
var filterService = require('../services/filtersService');
var appLogger = require('../logging/appLogger');
var config = require('../config/config.' + process.env.NODE_ENV);
router.get('/', function (req, res, next) {
    filterService.getAllFilterlist(function (err, response) {
        if (!err) {
            res.send(response);
        }
        else {
            res.status(500).send({ name: err.name, message: err.message });
        }
    });
});
router.get('/:type', function (req, res, next) {
    var query = { type: req.params.type };
    filterService.getFilter(query, function (error, response) {
        res.send(response);
    });
});

router.put('/:type', function (req, res, next) {
    var query = { type: req.params.type };
    filterService.updateFilterDetails(query, req.body, function (error, response) {
        res.send(response);
    });
});
router.post('/', function (req, res, next) {
    filterService.createFilter(req.body, function (error, response) {
        res.send(response);
    })
});
module.exports = router;
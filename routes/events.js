var express = require('express');
var router = express.Router();
var eventService = require('../services/eventService');
var appLogger = require('../logging/appLogger');
var config = require('../config/config.' + process.env.NODE_ENV);
var multer = require('multer');
const storage = require('multer-gridfs-storage')({
    url: config.dbConfig.url
});
const upload = multer({ storage: storage });
var gridfsDao = require('../daos/gridfsDao');

router.get('/', function (req, res, next) {
    eventService.getAllEventlist(function (err, response) {
        if (!err) {
            res.send(response);
        }
        else {
            res.status(500).send({ name: err.name, message: err.message });
        }
    });
});
router.get('/orgBy/:value', function (req, res, next) {
    var query={};
    query["orgBy.name"]=req.params.value;
    eventService.getEventsByQuery(query,function (err, response) {
        if (!err) {
            res.send(response);
        }
        else {
            res.status(500).send({ name: err.name, message: err.message });
        }
    });
});
router.get('/orgBody/:value', function (req, res, next) {
    var query={orgBody:req.params.value};
    eventService.getEventsByQuery(query,function (err, response) {
        if (!err) {
            res.send(response);
        }
        else {
            res.status(500).send({ name: err.name, message: err.message });
        }
    });
});
// insert student details
router.post('/', function (req, res, next) {
    eventService.createEvent(req.body, function (err, response) {
        if (!err) {
            appLogger.info("Successfully created a Event %s \\n", req.body.name);
            res.send(response);
        }
        else {
            appLogger.error(err, "Error while trying to create event %s \\n ", req.body.name);
            res.status(500).send({ name: err.name, message: err.message });
        }
    });
});
router.put('/updateEvent/:id', function (req, res, next) {
    eventService.editEvent(req.params.id, req.body, function (err, response) {
        if (!err) {
            appLogger.info("Successfully edited a Event %s \\n", req.body.name);
            res.send(response);
        }
        else {
            appLogger.error(err, "Error while trying to create event %s \\n ", req.body.name);
            res.status(500).send({ name: err.name, message: err.message });
        }
    });
});

router.post('/comment/:id', function (req, res, next) {
    eventService.postComment(req.params.id, req.body, function (err, response) {
        if (!err) {
            appLogger.info("Successfully posted a comment %s \\n", req.body.name);
            res.send(response);
        }
        else {
            appLogger.error(err, "Error while trying to post comment %s \\n ", req.body.name);
            res.status(500).send({ name: err.name, message: err.message });
        }
    });
});
router.get('/category/:value',function(req,res,next){
    var query={};
    query["category"]=req.params.value;
    eventService.getEventsByQuery(query,function(err,response){
        if(!err){
            res.send(response);
        }
        else{
            res.status(500).send({name:err.name,message:err.message});
        }
    });
});
router.post('/distinct/fields', function (req, res, next) {
    var query = req.body.query;
    var keyName = req.body.keyName;
    eventService.getDistinctValuesFromEvents(keyName, query, function (err, response) {
        if (!err) {
            res.send(response);
        }
        else {
            res.status(500).send({ name: err.name, message: err.message });
        }
    });
});
router.get('/:id', function (req, res, next) {

    eventService.getEventById(req.params.id, function (err, response) {
        if (!err) {
            res.send(response);
            appLogger.info("Successfully read a Student %d \\n", req.params.id);
        }
        else {
            appLogger.error(err, "Error while trying to read student %d \\n", req.params.id);
            res.status(500).send({ name: err.name, message: err.message });
        }
    });
});
router.put('/uploadPicture', upload.single("displayPicture"), function (req, res, next) {
    if (!req.file) {
        res.send({ message: "No files to upload" });
        return;
    }
    var attachmentDetails = {};
    var uploadedFile = req.file;
    attachmentDetails = {
        id: uploadedFile.id.toString(),
        contentType: uploadedFile.contentType,
        filename: uploadedFile.filename,
        originalname: uploadedFile.originalname,
        contentType: uploadedFile.contentType,
        size: uploadedFile.size
    };
    eventService.uploadDisplayPicture({ displayPicture: attachmentDetails }, function (err, response) {
        if (!err) {
            res.send(response);
        }
        else {
            res.status(500).send({ name: err.name, message: err.message });
        }
    });
});
router.get('/displayPicture/image/:type/:id/:originalname', function (req, res, next) {
    var attachmentDetails = {};
    attachmentDetails.contentType = "image/" + req.params.type;
    attachmentDetails.originalname = req.params.originalname;
    attachmentDetails.id = req.params.id;
    gridfsDao.openAttachment(attachmentDetails, res, function (err, response) {
        if (!err) {
            res.send(response);
        }
    });
});
router.delete('/displayPicture', function (req, res, next) {
    eventService.deletePictureinSavedImages(req.body, function (err, response) {
        if (!err) {
            eventService.deletePictureinFile(req.body, function (error, resp) {
                res.send(resp);
            });
        }
    });
});
router.post('/dirty/attachments', function (req, res, next) {
    eventService.removeDirtyAttachments(req.body.files, function (err, response) {
        if (!err) {
            res.send(response);
        }
        else {
            res.status(500).send({ name: err.name, message: err.message });
        }
    });
});
router.get('/distinctFilters/:type', function (req, res, next) {
    eventService.getDistinctValuesFromEvents(req.params.type, function (err, response) {
        if (!err) {
            res.send(response);
        }
    });
});
router.get('/distinctFilters/:type/:key/:value', function (req, res, next) {
    var query = {};
    query[req.params.key] = req.params.value;
    eventService.getDistinctValuesFromEvents(req.params.type, query, function (err, response) {
        if (!err) {
            res.send(response);
        }
        else {
            res.send(err);
        }
    });
});
router.get('/eventByVenue/:value/:startdate/:starttime', function (req, res, next) {
    var query = {};
    query["venue.name"] = req.params.value;
    query["startdate"] = req.params.startdate;
    query["starttime"] = req.params.starttime;
    eventService.getEventsByQuery(query, function (req, response) {
        res.send({ val: response.length });
    });
});
router.get('/studentDets/:val', function (req, res, next) {
    eventService.getStudentDetails(req.params.val, function (erro, response) {
        res.send(response);
    })
});
router.get('/facultyDets/:val', function (req, res, next) {
    eventService.getFacultyDetails(req.params.val, function (erro, response) {
        res.send(response);
    })
});
router.get('/report/:startdate',function(req,res,next){
    var query={};
    query["startdate"]=req.params.startdate;
    eventService.getEventsByQuery(query,function(req,response){
        res.send({val:response.length});
    })
})
module.exports = router;